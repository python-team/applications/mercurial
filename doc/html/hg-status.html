<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Docutils 0.21.2: https://docutils.sourceforge.io/" />
<title>hg status</title>
<link rel="stylesheet" href="../style.css" type="text/css" />
</head>
<body>
<div class="document" id="hg-status">
<span id="hg-status-1"></span>
<h1 class="title">hg status</h1>
<h2 class="subtitle" id="show-changed-files-in-the-working-directory">show changed files in the working directory</h2>

<div class="contents htmlonly topic" id="contents">
<p class="topic-title"><a class="reference internal" href="#top">Contents</a></p>
<ul class="simple">
<li><a class="reference internal" href="#synopsis" id="toc-entry-1">Synopsis</a></li>
<li><a class="reference internal" href="#description" id="toc-entry-2">Description</a></li>
<li><a class="reference internal" href="#options" id="toc-entry-3">Options</a></li>
<li><a class="reference internal" href="#aliases" id="toc-entry-4">Aliases</a></li>
</ul>
</div>
<div class="section" id="synopsis">
<h1><a class="toc-backref" href="#contents">Synopsis</a></h1>
<pre class="literal-block">
hg status [OPTION]... [FILE]...
</pre>
</div>
<div class="section" id="description">
<h1><a class="toc-backref" href="#contents">Description</a></h1>
<p>Show status of files in the repository. If names are given, only
files that match are shown. Files that are clean or ignored or
the source of a copy/move operation, are not listed unless
-c/--clean, -i/--ignored, -C/--copies or -A/--all are given.
Unless options described with &quot;show only ...&quot; are given, the
options -mardu are used.</p>
<p>Option -q/--quiet hides untracked (unknown and ignored) files
unless explicitly requested with -u/--unknown or -i/--ignored.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last"><a class="reference external" href="hg-status.html"><tt class="docutils literal">hg status</tt></a> may appear to disagree with diff if permissions have
changed or a merge has occurred. The standard diff format does
not report permission changes and diff only reports changes
relative to one merge parent.</p>
</div>
<p>If one revision is given, it is used as the base revision.
If two revisions are given, the differences between them are
shown. The --change option can also be used as a shortcut to list
the changed files of a revision from its first parent.</p>
<p>The codes used to show the status of files are:</p>
<pre class="literal-block">
M = modified
A = added
R = removed
C = clean
! = missing (deleted by non-hg command, but still tracked)
? = not tracked
I = ignored
  = origin of the previous file (with --copies)
</pre>
<div class="verbose docutils container">
<p>The -t/--terse option abbreviates the output by showing only the directory
name if all the files in it share the same status. The option takes an
argument indicating the statuses to abbreviate: 'm' for 'modified', 'a'
for 'added', 'r' for 'removed', 'd' for 'deleted', 'u' for 'unknown', 'i'
for 'ignored' and 'c' for clean.</p>
<p>It abbreviates only those statuses which are passed. Note that clean and
ignored files are not displayed with '--terse ic' unless the -c/--clean
and -i/--ignored options are also used.</p>
<p>The -v/--verbose option shows information when the repository is in an
unfinished merge, shelve, rebase state etc. You can have this behavior
turned on by default by enabling the <tt class="docutils literal">commands.status.verbose</tt> option.</p>
<p>You can skip displaying some of these states by setting
<tt class="docutils literal">commands.status.skipstates</tt> to one or more of: 'bisect', 'graft',
'histedit', 'merge', 'rebase', or 'unshelve'.</p>
<p>Template:</p>
<p>The following keywords are supported in addition to the common template
keywords and functions. See also <a class="reference external" href="hg.1.html#templates"><tt class="docutils literal">hg help templates</tt></a>.</p>
<table class="docutils field-list" frame="void" rules="none">
<col class="field-name" />
<col class="field-body" />
<tbody valign="top">
<tr class="field"><th class="field-name">path:</th><td class="field-body">String. Repository-absolute path of the file.</td>
</tr>
<tr class="field"><th class="field-name">source:</th><td class="field-body">String. Repository-absolute path of the file originated from.
Available if <tt class="docutils literal"><span class="pre">--copies</span></tt> is specified.</td>
</tr>
<tr class="field"><th class="field-name">status:</th><td class="field-body">String. Character denoting file's status.</td>
</tr>
</tbody>
</table>
<p>Examples:</p>
<ul>
<li><p class="first">show changes in the working directory relative to a
changeset:</p>
<pre class="literal-block">
hg status --rev 9353
</pre>
</li>
<li><p class="first">show changes in the working directory relative to the
current directory (see <a class="reference external" href="topic-patterns.html"><tt class="docutils literal">hg help patterns</tt></a> for more information):</p>
<pre class="literal-block">
hg status re:
</pre>
</li>
<li><p class="first">show all changes including copies in an existing changeset:</p>
<pre class="literal-block">
hg status --copies --change 9353
</pre>
</li>
<li><p class="first">get a NUL separated list of added files, suitable for xargs:</p>
<pre class="literal-block">
hg status -an0
</pre>
</li>
<li><p class="first">show more information about the repository status, abbreviating
added, removed, modified, deleted, and untracked paths:</p>
<pre class="literal-block">
hg status -v -t mardu
</pre>
</li>
</ul>
</div>
<p>Returns 0 on success.</p>
</div>
<div class="section" id="options">
<h1><a class="toc-backref" href="#contents">Options</a></h1>
<table class="docutils option-list" frame="void" rules="none">
<col class="option" />
<col class="description" />
<tbody valign="top">
<tr><td class="option-group">
<kbd><span class="option">-A</span>, <span class="option">--all</span></kbd></td>
<td>show status of all files</td></tr>
<tr><td class="option-group">
<kbd><span class="option">-m</span>, <span class="option">--modified</span></kbd></td>
<td>show only modified files</td></tr>
<tr><td class="option-group">
<kbd><span class="option">-a</span>, <span class="option">--added</span></kbd></td>
<td>show only added files</td></tr>
<tr><td class="option-group">
<kbd><span class="option">-r</span>, <span class="option">--removed</span></kbd></td>
<td>show only removed files</td></tr>
<tr><td class="option-group">
<kbd><span class="option">-d</span>, <span class="option">--deleted</span></kbd></td>
<td>show only missing files</td></tr>
<tr><td class="option-group">
<kbd><span class="option">-c</span>, <span class="option">--clean</span></kbd></td>
<td>show only files without changes</td></tr>
<tr><td class="option-group">
<kbd><span class="option">-u</span>, <span class="option">--unknown</span></kbd></td>
<td>show only unknown (not tracked) files</td></tr>
<tr><td class="option-group">
<kbd><span class="option">-i</span>, <span class="option">--ignored</span></kbd></td>
<td>show only ignored files</td></tr>
<tr><td class="option-group" colspan="2">
<kbd><span class="option">-n</span>, <span class="option">--no-status</span></kbd></td>
</tr>
<tr><td>&nbsp;</td><td>hide status prefix</td></tr>
<tr><td class="option-group" colspan="2">
<kbd><span class="option">-t</span>, <span class="option">--terse <var>&lt;VALUE&gt;</var></span></kbd></td>
</tr>
<tr><td>&nbsp;</td><td>show the terse output (EXPERIMENTAL) (default: nothing)</td></tr>
<tr><td class="option-group">
<kbd><span class="option">-C</span>, <span class="option">--copies</span></kbd></td>
<td>show source of copied files (DEFAULT: ui.statuscopies)</td></tr>
<tr><td class="option-group">
<kbd><span class="option">-0</span>, <span class="option">--print0</span></kbd></td>
<td>end filenames with NUL, for use with xargs</td></tr>
<tr><td class="option-group">
<kbd><span class="option">--rev <var>&lt;REV[+]&gt;</var></span></kbd></td>
<td>show difference from revision</td></tr>
<tr><td class="option-group">
<kbd><span class="option">--change <var>&lt;REV&gt;</var></span></kbd></td>
<td>list the changed files of a revision</td></tr>
<tr><td class="option-group" colspan="2">
<kbd><span class="option">-I</span>, <span class="option">--include <var>&lt;PATTERN[+]&gt;</var></span></kbd></td>
</tr>
<tr><td>&nbsp;</td><td>include names matching the given patterns</td></tr>
<tr><td class="option-group" colspan="2">
<kbd><span class="option">-X</span>, <span class="option">--exclude <var>&lt;PATTERN[+]&gt;</var></span></kbd></td>
</tr>
<tr><td>&nbsp;</td><td>exclude names matching the given patterns</td></tr>
<tr><td class="option-group">
<kbd><span class="option">-S</span>, <span class="option">--subrepos</span></kbd></td>
<td>recurse into subrepositories</td></tr>
<tr><td class="option-group" colspan="2">
<kbd><span class="option">-T</span>, <span class="option">--template <var>&lt;TEMPLATE&gt;</var></span></kbd></td>
</tr>
<tr><td>&nbsp;</td><td>display with template</td></tr>
</tbody>
</table>
<p>[+] marked option can be specified multiple times</p>
</div>
<div class="section" id="aliases">
<h1><a class="toc-backref" href="#contents">Aliases</a></h1>
<pre class="literal-block">
st
</pre>
</div>
</div>
</body>
</html>
