<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Docutils 0.21.2: https://docutils.sourceforge.io/" />
<title>Command-line flags</title>
<link rel="stylesheet" href="../style.css" type="text/css" />
</head>
<body>
<div class="document" id="command-line-flags">
<span id="topic-flags"></span>
<h1 class="title">Command-line flags</h1>

<div class="contents htmlonly topic" id="contents">
<p class="topic-title"><a class="reference internal" href="#top">Contents</a></p>
<ul class="simple">
<li><a class="reference internal" href="#flag-names" id="toc-entry-1">Flag names</a></li>
<li><a class="reference internal" href="#flag-types" id="toc-entry-2">Flag types</a></li>
<li><a class="reference internal" href="#specifying-flag-values" id="toc-entry-3">Specifying flag values</a></li>
<li><a class="reference internal" href="#specifying-boolean-flags" id="toc-entry-4">Specifying boolean flags</a></li>
<li><a class="reference internal" href="#specifying-list-flags" id="toc-entry-5">Specifying list flags</a></li>
<li><a class="reference internal" href="#setting-flag-defaults" id="toc-entry-6">Setting flag defaults</a></li>
<li><a class="reference internal" href="#overriding-flags-on-the-command-line" id="toc-entry-7">Overriding flags on the command line</a></li>
<li><a class="reference internal" href="#overriding-flag-defaults" id="toc-entry-8">Overriding flag defaults</a></li>
<li><a class="reference internal" href="#hidden-flags" id="toc-entry-9">Hidden flags</a></li>
</ul>
</div>
<p id="flags">Most Mercurial commands accept various flags.</p>
<div class="section" id="flag-names">
<h1><a class="toc-backref" href="#contents">Flag names</a></h1>
<p>Flags for each command are listed in <a class="reference external" href="hg-help.html"><tt class="docutils literal">hg help</tt></a> for that command.
Additionally, some flags, such as --repository, are global and can be used with
any command - those are seen in <a class="reference external" href="hg.1.html#-v"><tt class="docutils literal">hg help <span class="pre">-v</span></tt></a>, and can be specified before or
after the command.</p>
<p>Every flag has at least a long name, such as --repository. Some flags may also
have a short one-letter name, such as the equivalent -R. Using the short or long
name is equivalent and has the same effect. The long name may be abbreviated to
any unambiguous prefix. For example, <a class="reference external" href="hg-commit.html"><tt class="docutils literal">hg commit <span class="pre">--amend</span></tt></a> can be abbreviated
to <a class="reference external" href="hg-commit.html"><tt class="docutils literal">hg commit <span class="pre">--am</span></tt></a>.</p>
<p>Flags that have a short name can also be bundled together - for instance, to
specify both --edit (short -e) and --interactive (short -i), one could use:</p>
<pre class="literal-block">
hg commit -ei
</pre>
<p>If any of the bundled flags takes a value (i.e. is not a boolean), it must be
last, followed by the value:</p>
<pre class="literal-block">
hg commit -im 'Message'
</pre>
</div>
<div class="section" id="flag-types">
<h1><a class="toc-backref" href="#contents">Flag types</a></h1>
<p>Mercurial command-line flags can be strings, numbers, booleans, or lists of
strings.</p>
</div>
<div class="section" id="specifying-flag-values">
<h1><a class="toc-backref" href="#contents">Specifying flag values</a></h1>
<p>The following syntaxes are allowed, assuming a flag 'flagname' with short name
'f':</p>
<pre class="literal-block">
--flagname=foo
--flagname foo
-f foo
-ffoo
</pre>
<p>This syntax applies to all non-boolean flags (strings, numbers or lists).</p>
</div>
<div class="section" id="specifying-boolean-flags">
<h1><a class="toc-backref" href="#contents">Specifying boolean flags</a></h1>
<p>Boolean flags do not take a value parameter. To specify a boolean, use the flag
name to set it to true, or the same name prefixed with 'no-' to set it to
false:</p>
<pre class="literal-block">
hg commit --interactive
hg commit --no-interactive
</pre>
</div>
<div class="section" id="specifying-list-flags">
<h1><a class="toc-backref" href="#contents">Specifying list flags</a></h1>
<p>List flags take multiple values. To specify them, pass the flag multiple times:</p>
<pre class="literal-block">
hg files --include mercurial --include tests
</pre>
</div>
<div class="section" id="setting-flag-defaults">
<h1><a class="toc-backref" href="#contents">Setting flag defaults</a></h1>
<p>In order to set a default value for a flag in an hgrc file, it is recommended to
use aliases:</p>
<pre class="literal-block">
[alias]
commit = commit --interactive
</pre>
<p>For more information on hgrc files, see <a class="reference external" href="hgrc.5.html"><tt class="docutils literal">hg help config</tt></a>.</p>
</div>
<div class="section" id="overriding-flags-on-the-command-line">
<h1><a class="toc-backref" href="#contents">Overriding flags on the command line</a></h1>
<p>If the same non-list flag is specified multiple times on the command line, the
latest specification is used:</p>
<pre class="literal-block">
hg commit -m &quot;Ignored value&quot; -m &quot;Used value&quot;
</pre>
<p>This includes the use of aliases - e.g., if one has:</p>
<pre class="literal-block">
[alias]
committemp = commit -m &quot;Ignored value&quot;
</pre>
<p>then the following command will override that -m:</p>
<pre class="literal-block">
hg committemp -m &quot;Used value&quot;
</pre>
</div>
<div class="section" id="overriding-flag-defaults">
<h1><a class="toc-backref" href="#contents">Overriding flag defaults</a></h1>
<p>Every flag has a default value, and you may also set your own defaults in hgrc
as described above.
Except for list flags, defaults can be overridden on the command line simply by
specifying the flag in that location.</p>
</div>
<div class="section" id="hidden-flags">
<h1><a class="toc-backref" href="#contents">Hidden flags</a></h1>
<p>Some flags are not shown in a command's help by default - specifically, those
that are deemed to be experimental, deprecated or advanced. To show all flags,
add the --verbose flag for the help command:</p>
<pre class="literal-block">
hg help --verbose commit
</pre>
</div>
</div>
</body>
</html>
