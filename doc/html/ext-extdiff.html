<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Docutils 0.21.2: https://docutils.sourceforge.io/" />
<title>extdiff</title>
<link rel="stylesheet" href="../style.css" type="text/css" />
</head>
<body>
<div class="document" id="extdiff">
<span id="ext-extdiff"></span>
<h1 class="title">extdiff</h1>

<div class="contents htmlonly topic" id="contents">
<p class="topic-title"><a class="reference internal" href="#top">Contents</a></p>
<ul class="simple">
<li><a class="reference internal" href="#description" id="toc-entry-1">Description</a></li>
<li><a class="reference internal" href="#commands" id="toc-entry-2">Commands</a><ul>
<li><a class="reference internal" href="#file-content-management" id="toc-entry-3">File content management</a></li>
</ul>
</li>
</ul>
</div>
<p>command to allow external programs to compare revisions</p>
<div class="section" id="description">
<h1><a class="toc-backref" href="#contents">Description</a></h1>
<p>The extdiff Mercurial extension allows you to use external programs
to compare revisions, or revision with working directory. The external
diff programs are called with a configurable set of options and two
non-option arguments: paths to directories containing snapshots of
files to compare.</p>
<p>If there is more than one file being compared and the &quot;child&quot; revision
is the working directory, any modifications made in the external diff
program will be copied back to the working directory from the temporary
directory.</p>
<p>The extdiff extension also allows you to configure new diff commands, so
you do not need to type <a class="reference external" href="hg-extdiff.html"><tt class="docutils literal">hg extdiff <span class="pre">-p</span> kdiff3</tt></a> always.</p>
<pre class="literal-block">
[extdiff]
# add new command that runs GNU diff(1) in 'context diff' mode
cdiff = gdiff -Nprc5
## or the old way:
#cmd.cdiff = gdiff
#opts.cdiff = -Nprc5

# add new command called meld, runs meld (no need to name twice).  If
# the meld executable is not available, the meld tool in [merge-tools]
# will be used, if available
meld =

# add new command called vimdiff, runs gvimdiff with DirDiff plugin
# (see http://www.vim.org/scripts/script.php?script_id=102) Non
# English user, be sure to put &quot;let g:DirDiffDynamicDiffText = 1&quot; in
# your .vimrc
vimdiff = gvim -f &quot;+next&quot; \
          &quot;+execute 'DirDiff' fnameescape(argv(0)) fnameescape(argv(1))&quot;
</pre>
<p>Tool arguments can include variables that are expanded at runtime:</p>
<pre class="literal-block">
$parent1, $plabel1 - filename, descriptive label of first parent
$child,   $clabel  - filename, descriptive label of child revision
$parent2, $plabel2 - filename, descriptive label of second parent
$root              - repository root
$parent is an alias for $parent1.
</pre>
<p>The extdiff extension will look in your [diff-tools] and [merge-tools]
sections for diff tool arguments, when none are specified in [extdiff].</p>
<pre class="literal-block">
[extdiff]
kdiff3 =

[diff-tools]
kdiff3.diffargs=--L1 '$plabel1' --L2 '$clabel' $parent $child
</pre>
<p>If a program has a graphical interface, it might be interesting to tell
Mercurial about it. It will prevent the program from being mistakenly
used in a terminal-only environment (such as an SSH terminal session),
and will make <a class="reference external" href="hg-extdiff.html"><tt class="docutils literal">hg extdiff <span class="pre">--per-file</span></tt></a> open multiple file diffs at once
instead of one by one (if you still want to open file diffs one by one,
you can use the --confirm option).</p>
<p>Declaring that a tool has a graphical interface can be done with the
<tt class="docutils literal">gui</tt> flag next to where <tt class="docutils literal">diffargs</tt> are specified:</p>
<pre class="literal-block">
[diff-tools]
kdiff3.diffargs=--L1 '$plabel1' --L2 '$clabel' $parent $child
kdiff3.gui = true
</pre>
<p>You can use -I/-X and list of file or directory names like normal
<a class="reference external" href="hg-diff.html"><tt class="docutils literal">hg diff</tt></a> command. The extdiff extension makes snapshots of only
needed files, so running the external diff program will actually be
pretty fast (at least faster than having to compare the entire tree).</p>
</div>
<div class="section" id="commands">
<h1><a class="toc-backref" href="#contents">Commands</a></h1>
<div class="section" id="file-content-management">
<h2><a class="toc-backref" href="#contents">File content management</a></h2>
<div class="section" id="extdiff-1">
<h3>extdiff</h3>
<p>use external program to diff repository (or selected files):</p>
<pre class="literal-block">
hg extdiff [OPT]... [FILE]...
</pre>
<p>Show differences between revisions for the specified files, using
an external program. The default program used is diff, with
default options &quot;-Npru&quot;.</p>
<p>To select a different program, use the -p/--program option. The
program will be passed the names of two directories to compare,
unless the --per-file option is specified (see below). To pass
additional options to the program, use -o/--option. These will be
passed before the names of the directories or files to compare.</p>
<p>The --from, --to, and --change options work the same way they do for
<a class="reference external" href="hg-diff.html"><tt class="docutils literal">hg diff</tt></a>.</p>
<p>The --per-file option runs the external program repeatedly on each
file to diff, instead of once on two directories. By default,
this happens one by one, where the next file diff is open in the
external program only once the previous external program (for the
previous file diff) has exited. If the external program has a
graphical interface, it can open all the file diffs at once instead
of one by one. See <a class="reference external" href="hg.1.html#-e"><tt class="docutils literal">hg help <span class="pre">-e</span> extdiff</tt></a> for information about how
to tell Mercurial that a given program has a graphical interface.</p>
<p>The --confirm option will prompt the user before each invocation of
the external program. It is ignored if --per-file isn't specified.</p>
<p>Options:</p>
<table class="docutils option-list" frame="void" rules="none">
<col class="option" />
<col class="description" />
<tbody valign="top">
<tr><td class="option-group" colspan="2">
<kbd><span class="option">-p</span>, <span class="option">--program <var>&lt;CMD&gt;</var></span></kbd></td>
</tr>
<tr><td>&nbsp;</td><td>comparison program to run</td></tr>
<tr><td class="option-group" colspan="2">
<kbd><span class="option">-o</span>, <span class="option">--option <var>&lt;OPT[+]&gt;</var></span></kbd></td>
</tr>
<tr><td>&nbsp;</td><td>pass option to comparison program</td></tr>
<tr><td class="option-group" colspan="2">
<kbd><span class="option">-r</span>, <span class="option">--rev <var>&lt;REV[+]&gt;</var></span></kbd></td>
</tr>
<tr><td>&nbsp;</td><td>revision (DEPRECATED)</td></tr>
<tr><td class="option-group">
<kbd><span class="option">--from <var>&lt;REV1&gt;</var></span></kbd></td>
<td>revision to diff from</td></tr>
<tr><td class="option-group">
<kbd><span class="option">--to <var>&lt;REV2&gt;</var></span></kbd></td>
<td>revision to diff to</td></tr>
<tr><td class="option-group" colspan="2">
<kbd><span class="option">-c</span>, <span class="option">--change <var>&lt;REV&gt;</var></span></kbd></td>
</tr>
<tr><td>&nbsp;</td><td>change made by revision</td></tr>
<tr><td class="option-group">
<kbd><span class="option">--per-file</span></kbd></td>
<td>compare each file instead of revision snapshots</td></tr>
<tr><td class="option-group">
<kbd><span class="option">--confirm</span></kbd></td>
<td>prompt user before each external program invocation</td></tr>
<tr><td class="option-group">
<kbd><span class="option">--patch</span></kbd></td>
<td>compare patches for two revisions</td></tr>
<tr><td class="option-group" colspan="2">
<kbd><span class="option">-I</span>, <span class="option">--include <var>&lt;PATTERN[+]&gt;</var></span></kbd></td>
</tr>
<tr><td>&nbsp;</td><td>include names matching the given patterns</td></tr>
<tr><td class="option-group" colspan="2">
<kbd><span class="option">-X</span>, <span class="option">--exclude <var>&lt;PATTERN[+]&gt;</var></span></kbd></td>
</tr>
<tr><td>&nbsp;</td><td>exclude names matching the given patterns</td></tr>
<tr><td class="option-group">
<kbd><span class="option">-S</span>, <span class="option">--subrepos</span></kbd></td>
<td>recurse into subrepositories</td></tr>
</tbody>
</table>
<p>[+] marked option can be specified multiple times</p>
</div>
</div>
</div>
</div>
</body>
</html>
