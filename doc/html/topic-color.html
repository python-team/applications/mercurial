<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Docutils 0.21.2: https://docutils.sourceforge.io/" />
<title>Colorizing Outputs</title>
<link rel="stylesheet" href="../style.css" type="text/css" />
</head>
<body>
<div class="document" id="colorizing-outputs">
<span id="topic-color"></span>
<h1 class="title">Colorizing Outputs</h1>

<div class="contents htmlonly topic" id="contents">
<p class="topic-title"><a class="reference internal" href="#top">Contents</a></p>
<ul class="simple">
<li><a class="reference internal" href="#mode" id="toc-entry-1">Mode</a></li>
<li><a class="reference internal" href="#effects" id="toc-entry-2">Effects</a></li>
<li><a class="reference internal" href="#labels" id="toc-entry-3">Labels</a></li>
<li><a class="reference internal" href="#custom-colors" id="toc-entry-4">Custom colors</a></li>
</ul>
</div>
<p id="color">Mercurial colorizes output from several commands.</p>
<p>For example, the diff command shows additions in green and deletions
in red, while the status command shows modified files in magenta. Many
other commands have analogous colors. It is possible to customize
these colors.</p>
<p>To enable color (default) whenever possible use:</p>
<pre class="literal-block">
[ui]
color = yes
</pre>
<p>To disable color use:</p>
<pre class="literal-block">
[ui]
color = no
</pre>
<p>See <a class="reference external" href="hgrc.5.html#ui"><tt class="docutils literal">hg help config.ui.color</tt></a> for details.</p>
<div class="windows docutils container">
The default pager on Windows does not support color, so enabling the pager
will effectively disable color.  See <a class="reference external" href="hgrc.5.html#ui"><tt class="docutils literal">hg help config.ui.paginate</tt></a> to disable
the pager.  Alternately, MSYS and Cygwin shells provide <cite>less</cite> as a pager,
which can be configured to support ANSI color mode.  Windows 10 natively
supports ANSI color mode.</div>
<div class="section" id="mode">
<h1><a class="toc-backref" href="#contents">Mode</a></h1>
<p>Mercurial can use various systems to display color. The supported modes are
<tt class="docutils literal">ansi</tt>, <tt class="docutils literal">win32</tt>, and <tt class="docutils literal">terminfo</tt>.  See <a class="reference external" href="hgrc.5.html#color"><tt class="docutils literal">hg help config.color</tt></a> for details
about how to control the mode.</p>
</div>
<div class="section" id="effects">
<h1><a class="toc-backref" href="#contents">Effects</a></h1>
<p>Other effects in addition to color, like bold and underlined text, are
also available. By default, the terminfo database is used to find the
terminal codes used to change color and effect.  If terminfo is not
available, then effects are rendered with the ECMA-48 SGR control
function (aka ANSI escape codes).</p>
<p>The available effects in terminfo mode are 'blink', 'bold', 'dim',
'inverse', 'invisible', 'italic', 'standout', and 'underline'; in
ECMA-48 mode, the options are 'bold', 'inverse', 'italic', and
'underline'.  How each is rendered depends on the terminal emulator.
Some may not be available for a given terminal type, and will be
silently ignored.</p>
<p>If the terminfo entry for your terminal is missing codes for an effect
or has the wrong codes, you can add or override those codes in your
configuration:</p>
<pre class="literal-block">
[color]
terminfo.dim = \E[2m
</pre>
<p>where 'E' is substituted with an escape character.</p>
</div>
<div class="section" id="labels">
<h1><a class="toc-backref" href="#contents">Labels</a></h1>
<p>Text receives color effects depending on the labels that it has. Many
default Mercurial commands emit labelled text. You can also define
your own labels in templates using the label function, see <a class="reference external" href="hg.1.html#templates"><tt class="docutils literal">hg help
templates</tt></a>. In order to receive effects, labels must have a dot, such
as <cite>log.secret</cite> or <cite>branch.active</cite>. A single portion of text may have
more than one label. In that case, effects given to the last label
will override any other effects. This includes the special &quot;none&quot;
effect, which nullifies other effects.</p>
<p>Labels are normally invisible. In order to see these labels and their
position in the text, use the global --color=debug option. The same
anchor text may be associated to multiple labels, e.g.</p>
<blockquote>
[log.changeset changeset.secret|changeset:   22611:6f0a53c8f587]</blockquote>
<p>The following are the default effects for some default labels. Default
effects may be overridden from your configuration file:</p>
<pre class="literal-block">
[color]
status.modified = blue bold underline red_background
status.added = green bold
status.removed = red bold blue_background
status.deleted = cyan bold underline
status.unknown = magenta bold underline
status.ignored = black bold

# 'none' turns off all effects
status.clean = none
status.copied = none

qseries.applied = blue bold underline
qseries.unapplied = black bold
qseries.missing = red bold

diff.diffline = bold
diff.extended = cyan bold
diff.file_a = red bold
diff.file_b = green bold
diff.hunk = magenta
diff.deleted = red
diff.inserted = green
diff.changed = white
diff.tab =
diff.trailingwhitespace = bold red_background

# Blank so it inherits the style of the surrounding label
changeset.public =
changeset.draft =
changeset.secret =

resolve.unresolved = red bold
resolve.resolved = green bold

bookmarks.active = green

branches.active = none
branches.closed = black bold
branches.current = green
branches.inactive = none

tags.normal = green
tags.local = black bold

rebase.rebased = blue
rebase.remaining = red bold

shelve.age = cyan
shelve.newest = green bold
shelve.name = blue bold

histedit.remaining = red bold
</pre>
</div>
<div class="section" id="custom-colors">
<h1><a class="toc-backref" href="#contents">Custom colors</a></h1>
<p>Because there are only eight standard colors, Mercurial allows you
to define color names for other color slots which might be available
for your terminal type, assuming terminfo mode.  For instance:</p>
<pre class="literal-block">
color.brightblue = 12
color.pink = 207
color.orange = 202
</pre>
<p>to set 'brightblue' to color slot 12 (useful for 16 color terminals
that have brighter colors defined in the upper eight) and, 'pink' and
'orange' to colors in 256-color xterm's default color cube.  These
defined colors may then be used as any of the pre-defined eight,
including appending '_background' to set the background to that color.</p>
</div>
</div>
</body>
</html>
